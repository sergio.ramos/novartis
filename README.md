# Novartis front end

descrição do projeto

## Como rodar o projeto

O projeto existe duas camadas, a de backend a de frontend.

## Rodando backend

Para rodar a parte de backend é necessário ou configurar um servidor no windows ou usar docker com docker-compose. Em ambos os casos é obrigatório passar por esses passos:

- instalar dependências usando composer dentro do da pasta backend usando o comando `composer install`
- rodar o servidor novamente, `docker-composer up` na raiz do projeto para quem estiver usando docker.


## Compilando front end

Para rodar a parte de frontend basta ter instalado o [nodejs](https://nodejs.org/en/) e [yarn](https://yarnpkg.com/).

- Entrar na pasta tools
- Rodar o comando `yarn` para instalar as dependências
- Rodar o comando `yarn dev:build` para compilar o projeto
- Abrir o [localhost](http://localhost:8080)

## Para configurar as cores do Tema:

- Entrar na pasta src/sass/components
- Acessar o arquivo _variables.demo.scss
- Editar as cores

OBS: 
- As cores padrão do tema estão todas nos arquivos _variables.bootstrap.sass e _variables.custom.sass. 
- Para editar basta copiar as váriaveis de cor que deseja alterar, e colar no arquivo _variables.demo.scss. 
- Só deve ser editado no arquivo _variables.demo.sass
- Não deve incluir o !default ou o !important.

## Para configurar as cores dos componentes: 

- Entrar na pasta src/sass/layout
- Acessar o arquivo _variables.demo.scss
- Editar as cores


## Alteração de templates

O template novartis foi baseado em um template Metronic. Toda a estilização é feita atraves dos arquivos
sass. Para alterar o template Novartis, basta acessar o arquivo `/backend/src/sass/novartis-style.scss`. 
Todos os estilos personalizados estão sendo importados dentro do mesmo.

## Alterar estilo de uma pagina ou componente

Para alterar o estilo de uma pagina ou componente, basta criar um arquivo com as ateraçãoes
dentro de `/backend/src/sass/pages` ou `/backend/src/sass/components` para componentes. Apos criar o estilo
fazer a importação dos arquivos criados dentro do `novartis-style.scss`.