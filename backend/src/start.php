<?php

namespace Novartis;

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

$container = new Container();
AppFactory::setContainer($container);

// Set view in Container
$container->set('view', function () {
    return Twig::create(__DIR__ . '/../area-logada', []);
});

$app = AppFactory::create();

// Add Twig-View Middleware
$app->add(TwigMiddleware::createFromContainer($app));

$app->get('/', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/initial-home.twig');
});

$app->get('/admin/home', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/home.twig');
});

$app->get('/admin/visita-virtual', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/visita-virtual.twig');
});

$app->get('/admin/conferencias', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/conferencias.twig');
});

$app->get('/admin/reunioes', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/reunioes.twig');
});

$app->get('/admin/chat', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/chat.twig');
});

$app->get('/admin/area-pessoal', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'pages/area-pessoal.twig');
});

$app->get('/admin/gestao-site', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'administrador/gestao-site.twig');
});

$app->get('/admin/analytics', function (Request $request, Response $response, array $args) {
    return $this->get('view')->render($response, 'administrador/analytics.twig');
});

$app->run();
