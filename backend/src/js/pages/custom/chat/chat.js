let itemSelected = null
function selectPeaple(item) {
	const listToSelect = document.getElementsByClassName('nv-chat-user_selected');
  const itemToSelect = document.getElementById(item);
  itemSelected = itemToSelect
  if (listToSelect[0]) {
    listToSelect[0].classList.remove('nv-chat-user_selected');
  }

  if (itemToSelect) {
    renderContentItem(itemToSelect)
  	itemToSelect.classList.add('nv-chat-user_selected');
	}
}

function getNameFromItem(item) {
  return item.children[0].children[1].children[0].innerHTML
}

function getContentChat(title) {
  const content = `
    <!--begin::Header-->
    <div class="card-header align-items-center px-4 py-3">
        <div class="d-flex flex-row text-left flex-grow-2">
            <div class="symbol symbol-circle symbol-40 mr-3">
              <img alt="Pic" src="${getImageFromItem(itemSelected)}">
            </div>
            <div class="text-dark-75 font-weight-bold font-size-h5">${title}</div>
        </div>
    </div>

    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body">
        <div class="nv-scroll-content-list">
            <!--begin::Scroll-->
            <div id="content-message-list" class="scroll scroll-pull" data-height="375" data-mobile-height="300">

            </div>
            <!--end::Scroll-->
        </div>
    </div>

    <!--end::Body-->

    <!--begin::Footer-->
    <div class="card-footer align-items-center">

        <!--begin::Compose-->
        <textarea id="message-to-send" class="form-control border-0 p-0" rows="2" placeholder="Escreva uma mensagem"></textarea>
        <div class="d-flex align-items-center justify-content-end mt-5">
           <div>
                <button type="button" onClick="sendMessage()" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Enviar</button>
            </div>
        </div>

        <!--begin::Compose-->
    </div>

    <!--end::Footer-->

<!--end::Card-->

  `
  return content

 }

  function renderMessage(type, message, picture, name) {
      switch (type) {
        case 'in':
          return `
        <div class="d-flex flex-column mb-5 align-items-start">
          <div class="d-flex align-items-center">
            <div class="symbol symbol-circle symbol-40 mr-3">
              <img alt="foto do usuario" src="${picture}">
            </div>
              <div>
             <div class="d-flex flex-column align-items-start">
                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">${name}</a>
                <span class="text-muted font-size-sm">1 minutes</span>
               </div>
          </div>
          </div>
            <div class="mt-2 rounded p-5 bg-gray-300 text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">${message}</div>
      </div>
      `
      case 'out':
        return `
          <div class="d-flex flex-column mb-5 align-items-end">
            <div class="d-flex align-items-center">
              <div class="d-flex flex-column align-items-end">
                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">Você</a>
                <span class="text-muted font-size-sm">3 minutes</span>
              </div>
                    <div class="symbol symbol-circle symbol-40 ml-3">
                      <img src="../assets/images/perfil-foto.png" alt="Foto de perfil">
                    </div>
                  </div>
                    <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">${message}</div>
                      </div>
        `
      }
  }
 
  function getImageFromItem(item) {
    return item.children[0].children[0].children[0].src
  }

  function sendMessage() {
    const message = document.getElementById("message-to-send")
    const image = getImageFromItem(itemSelected)
    const name = getNameFromItem(itemSelected)
    const messageHtml = renderMessage('in', message.value, image, name)
    message.value = ''
    const div = document.createElement('div')
    div.innerHTML = messageHtml
    const chatBase = document.getElementById('content-message-list')
    chatBase.appendChild(div)
  }

 function renderContentItem(item) {
  const name = getNameFromItem(item)
  const chat = document.getElementById('chat-content')

  chat.innerHTML = getContentChat(name)
}

function renderImageItem(item) {}

function renderNameItem(item) {}

jQuery(document).ready(function () {
      selectPeaple;
      sendMessage
});
