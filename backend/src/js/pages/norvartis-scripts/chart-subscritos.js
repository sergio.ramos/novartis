"use strict";

// Shared Colors Definition
const primary = '#0460A9';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

var KTApexChartsDemo = function () {
	// Private functions

	// subscritos chart
	var _chartSubscritos = function () {
		const apexChart = "#chart_subescritos";
		var options = {
			series: [{
				name: 'Traffic',
				data: [2000, 3000, 2250, 2500, 2750, 2500, 2750, 2500, 2000, 2750, 2500, 2250]
			}, {
				name: 'Sales',
				data: [3750, 4000, 3250, 3500, 3750, 4000, 4250, 4500, 3500, 4750, 4000, 3750]
			}],
			chart: {
				height: 450,
				type: 'area'
			},
			plotOptions: {
				bar: {
					horizontal: true,
				},
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				curve: 'straight',
				width: 3
			},
			xaxis: {
				type: 'datetime',
				categories: [
					"2020-08-20T00:00:00.000Z", "2020-08-20T02:00:00.000Z", 
					"2020-08-20T04:00:00.000Z", "2020-08-20T06:00:00.000Z", 
					"2020-08-20T08:00:00.000Z", "2020-08-20T10:00:00.000Z", 
					"2020-08-20T12:00:00.000Z", "2020-08-20T14:00:00.000Z",
					"2020-08-20T16:00:00.000Z", "2020-08-20T18:00:00.000Z",
					"2020-08-20T20:00:00.000Z", "2020-08-20T22:00:00.000Z"
				],
				labels: {
					format: "HH TT"
				}
			},
			tooltip: {
				x: {
					format: 'dd/MM/yy HH:mm'
				},
			},
			legend: {
				position: 'top',
				horizontalAlign: 'left',
				offsetX: 0
			},
			colors: [danger, primary]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}
	// subscritos chart

	//users chart
	var _chartUsers = function () {
		const apexChart = "#chart_users";
		var options = {
			series: [{
				name: 'Traffic',
				data: [2000, 3000, 2250, 2500, 2750, 2500, 2750, 2500, 2000, 2750, 2500, 2250]
			}, {
				name: 'Sales',
				data: [3750, 4000, 3250, 3500, 3750, 4000, 4250, 4500, 3500, 4750, 4000, 3750]
			}],
			chart: {
				height: 450,
				type: 'area'
			},
			plotOptions: {
				bar: {
					horizontal: true,
				},
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				curve: 'straight',
				width: 3
			},
			xaxis: {
				type: 'datetime',
				categories: [
					"2020-08-20T00:00:00.000Z", "2020-08-20T02:00:00.000Z", 
					"2020-08-20T04:00:00.000Z", "2020-08-20T06:00:00.000Z", 
					"2020-08-20T08:00:00.000Z", "2020-08-20T10:00:00.000Z", 
					"2020-08-20T12:00:00.000Z", "2020-08-20T14:00:00.000Z",
					"2020-08-20T16:00:00.000Z", "2020-08-20T18:00:00.000Z",
					"2020-08-20T20:00:00.000Z", "2020-08-20T22:00:00.000Z"
				],
				labels: {
					format: "HH TT"
				}
			},
			tooltip: {
				x: {
					format: 'dd/MM/yy HH:mm'
				},
			},
			legend: {
				position: 'top',
				horizontalAlign: 'left',
				offsetX: 0
			},
			colors: [danger, primary]
		};
		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}
	//users chart

	return {
		// public functions
		init: function () {
			_chartSubscritos();
			_chartUsers();
		}
	};
}();

jQuery(document).ready(function () {
	KTApexChartsDemo.init();
});
