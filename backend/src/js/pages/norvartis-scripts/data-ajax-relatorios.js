"use strict";
// Class definition

var KTDatatableRemoteAjaxDemo = function() {
    // Private functions

    // Test Relatórios Json
    var jsonRelatoriosTest = JSON.parse(
        `[
            {
                "UserID":1,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":2,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":3,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":4,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":5,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":6,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":7,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":8,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":9,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":10,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":11,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":12,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":13,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":14,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":15,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            }
        ]`
    )

    // basic Relatórios table
    var tableRelatorios = function() {

        var datatableRelatorios = $('#kt_datatable_relatorios').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: jsonRelatoriosTest,
                pageSize: 2
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                height: 500,
                class: 'relatorios-datatable',
                spinner: {
                    overlayColor: '#0460A9',
                    message: 'Carregando..'
                }
            },

            translate: {
                records : {
                    processing: 'Por favor, espere...',
                    noRecords: 'sem registros'
                }
            },

            // column sorting
            sortable: true,

            pagination: false,

            // columns definition
            columns: [
                {
                field: 'UserID',
                title: '#',
                sortable: false,
                width: 30,
                type: 'number',
                textAlign: 'center',
                class: 'kt-ID'
            }, 
            {
                field: 'Data',
                title: 'Data',
                type: 'date',
                format: 'DD/MM/YYYY',
                class: 'kt-data',
                width: 90
            }, 
            {
                field: 'Name',
                title: 'Nome Dr.',
                class: 'kt-name',
                sortable: false,
                width: 130
            },
            {
                field: 'Description',
                title: 'Descrição.',
                class: 'kt-description',
                sortable: false,
                width: 370
            },
            {
                field: 'Phone',
                title: 'Telemóvel.',
                class: 'kt-phone',
                sortable: false,
                width: 130
            },
            {
                field: 'Actions',
                title: 'Ação',
                sortable: false,
                width: 80,
                overflow: 'visible',
                autoHide: false,
                class: 'kt-actions',
                template: function() {
                    return '\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-lg fas fa-edit text-primary"></i>\
                            </span>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-md flaticon2-cross text-danger"></i>\
                            </span>\
                        </a>\
                    ';
                },
            }],

        });
    };

    // *******************************************************************//

    // Test Subscritos Json
    var jsonSubscritos = JSON.parse(
        `[
            {
                "UserID":1,
                "Name":"Ana Matos",
                "Especialization":"Medicina Interna"
            },
            {
                "UserID":2,
                "Name":"Rui Cardoso",
                "Especialization":"Medicina Interna"
            },
            {
                "UserID":3,
                "Name":"João Silva",
                "Especialization":"Medicina Interna"
            }
        ]`
    )

    // basic Subscritos table
    var tableSubscritos = function() {

        var datatableSubscritos = $('#kt_datatable_subscritos').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: jsonSubscritos,
                pageSize: 2
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                height: 250,
                class: 'subscritos-datatable',
                spinner: {
                    overlayColor: '#0460A9',
                    message: 'Carregando..'
                }
            },

            translate: {
                records : {
                    processing: 'Por favor, espere...',
                    noRecords: 'sem registros'
                }
            },

            // column sorting
            sortable: true,

            pagination: false,

            // columns definition
            columns: [
                {
                    field: 'UserID',
                    title: '#',
                    sortable: false,
                    width: 30,
                    type: 'number',
                    textAlign: 'center',
                    class: 'kt-ID', 
                    visible: false
                },
                {
                    field: 'Name',
                    title: 'Nome',
                    class: 'kt-name',
                    sortable: false,
                    width: 110
                },
                {
                    field: 'Especialization',
                    title: 'Especialização.',
                    class: 'kt-Especialization',
                    sortable: false,
                    width: 110
                }
            ],

        });
    };

    // *******************************************************************//

    // basic Users table
    var tableUsers = function() {

        var datatableUsers = $('#kt_datatable_users').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: jsonSubscritos,
                pageSize: 2
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                height: 250,
                class: 'users-datatable',
                spinner: {
                    overlayColor: '#0460A9',
                    message: 'Carregando..'
                }
            },

            translate: {
                records : {
                    processing: 'Por favor, espere...',
                    noRecords: 'sem registros'
                }
            },

            // column sorting
            sortable: true,

            pagination: false,

            // columns definition
            columns: [
                {
                    field: 'UserID',
                    title: '#',
                    sortable: false,
                    width: 30,
                    type: 'number',
                    textAlign: 'center',
                    class: 'kt-ID', 
                    visible: false
                },
                {
                    field: 'Name',
                    title: 'Nome',
                    class: 'kt-name',
                    sortable: false,
                    width: 80
                },
                {
                    field: 'Especialization',
                    title: 'Especialização.',
                    class: 'kt-Especialization',
                    sortable: false,
                    width: 100
                }
            ],

        });
    };

    return {
        // public functions
        init: function() {
            tableRelatorios();
            tableSubscritos();
            tableUsers();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableRemoteAjaxDemo.init();
});
