"use strict";
// Class definition

var KTDatatableRemoteAjaxDemo = function() {
    // Private functions

    // Test area pessoal Json
    var jsonAreaPessoalTest = JSON.parse(
        `[
            {
                "ReuniaoID":1,
                "Titulo":"Dr. Rui Cardoso",
                "EventDate":"12 Jul 2020 11:45",
                "Actions":null
            },
            {
                "ReuniaoID":2,
                "Titulo":"Dr. Paulo Sousa",
                "EventDate":"12 Jul 2020 15:00",
                "Actions":null
            },
            {
                "ReuniaoID":3,
                "Titulo":"Dr. Rui Cardoso",
                "EventDate":"12 Jul 2020 11:45",
                "Actions":null
            },
            {
                "ReuniaoID":4,
                "Titulo":"Dr. Paulo Sousa",
                "EventDate":"12 Jul 2020 15:00",
                "Actions":null
            }
        ]`
    )

    // basic area pessoal table
    var tableAreaPessoal = function() {

        var datatablePessoal = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: jsonAreaPessoalTest,
                pageSize: 2
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                height: 200,
                class: 'reuniao-datatable',
                spinner: {
                    overlayColor: '#0460A9',
                    message: 'Carregando..'
                }
            },

            translate: {
                records : {
                    processing: 'Por favor, espere...',
                    noRecords: 'sem registros'
                }
            },

            // column sorting
            sortable: true,

            pagination: false,

            // columns definition
            columns: [
                {
                field: 'ReuniaoID',
                title: '#',
                sortable: 'asc',
                width: 30,
                type: 'number',
                selector: false,
                textAlign: 'center',
                class: 'kt-ID',
                visible: true
            }, 
            {
                field: 'Titulo',
                title: 'Titulo',
                class: 'kt-Titulo'
            }, 
            {
                field: 'EventDate',
                title: 'Data',
                type: 'date',
                format: 'DD/MM/YYYY',
                class: 'kt-EventDate'
            }, 
            {
                field: 'Actions',
                title: 'Ação',
                sortable: false,
                width: 80,
                overflow: 'visible',
                autoHide: false,
                template: function() {
                    return '\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-lg fas fa-edit text-primary"></i>\
                            </span>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-md flaticon2-cross text-danger"></i>\
                            </span>\
                        </a>\
                    ';
                },
            }],

        });
    };

    return {
        // public functions
        init: function() {
            tableAreaPessoal();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableRemoteAjaxDemo.init();
});
