"use strict";
// Class definition

var KTDatatableRemoteAjaxDemo = function() {
    // Private functions

    // Test area pessoal Json
    var jsonAreaPessoalTest = JSON.parse(
        `[
            {
                "ReuniaoID":1,
                "Titulo":"Dr. Rui Cardoso",
                "EventDate":"12 Jul 2020 11:45",
                "Actions":null
            },
            {
                "ReuniaoID":2,
                "Titulo":"Dr. Paulo Sousa",
                "EventDate":"12 Jul 2020 15:00",
                "Actions":null
            },
            {
                "ReuniaoID":3,
                "Titulo":"Dr. Rui Cardoso",
                "EventDate":"12 Jul 2020 11:45",
                "Actions":null
            },
            {
                "ReuniaoID":4,
                "Titulo":"Dr. Paulo Sousa",
                "EventDate":"12 Jul 2020 15:00",
                "Actions":null
            }
        ]`
    )

    // basic area pessoal table
    var tableAreaPessoal = function() {

        var datatablePessoal = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: jsonAreaPessoalTest,
                pageSize: 2
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                height: 200,
                class: 'reuniao-datatable',
                spinner: {
                    overlayColor: '#0460A9',
                    message: 'Carregando..'
                }
            },

            translate: {
                records : {
                    processing: 'Por favor, espere...',
                    noRecords: 'sem registros'
                }
            },

            // column sorting
            sortable: true,

            pagination: false,

            // columns definition
            columns: [
                {
                field: 'ReuniaoID',
                title: '#',
                sortable: 'asc',
                width: 30,
                type: 'number',
                selector: false,
                textAlign: 'center',
                class: 'kt-ID',
                visible: true
            }, 
            {
                field: 'Titulo',
                title: 'Titulo',
                class: 'kt-Titulo'
            }, 
            {
                field: 'EventDate',
                title: 'Data',
                type: 'date',
                format: 'DD/MM/YYYY',
                class: 'kt-EventDate'
            }, 
            {
                field: 'Actions',
                title: 'Ação',
                sortable: false,
                width: 80,
                overflow: 'visible',
                autoHide: false,
                template: function() {
                    return '\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-lg fas fa-edit text-primary"></i>\
                            </span>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-md flaticon2-cross text-danger"></i>\
                            </span>\
                        </a>\
                    ';
                },
            }],

        });
    };


    // Test Relatórios Json
    var jsonRelatoriosTest = JSON.parse(
        `[
            {
                "UserID":1,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":2,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":3,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":4,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":5,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":6,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":7,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":8,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":9,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":10,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":11,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":12,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":13,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":14,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            },
            {
                "UserID":15,
                "Name":"Dr. Rui Cardoso",
                "Data":"12 Jul 2020 11:45",
                "Actions":null,
                "Description":"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy",
                "Phone":"(351) 902-013-6830"
            }
        ]`
    )

    // basic Relatórios table
    var tableRelatorios = function() {

        var datatableRelatorios = $('#kt_datatable_relatorios').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: jsonRelatoriosTest,
                pageSize: 2
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                height: 500,
                class: 'relatorios-datatable',
                spinner: {
                    overlayColor: '#0460A9',
                    message: 'Carregando..'
                }
            },

            translate: {
                records : {
                    processing: 'Por favor, espere...',
                    noRecords: 'sem registros'
                }
            },

            // column sorting
            sortable: true,

            pagination: false,

            // columns definition
            columns: [
                {
                field: 'UserID',
                title: '#',
                sortable: false,
                width: 30,
                type: 'number',
                textAlign: 'center',
                class: 'kt-ID'
            }, 
            {
                field: 'Data',
                title: 'Data',
                type: 'date',
                format: 'DD/MM/YYYY',
                class: 'kt-data',
                width: 110
            }, 
            {
                field: 'Name',
                title: 'Nome Dr.',
                class: 'kt-name',
                width: 130
            },
            {
                field: 'Description',
                title: 'Descrição.',
                class: 'kt-description',
                width: 350
            },
            {
                field: 'Phone',
                title: 'Telemóvel.',
                class: 'kt-phone',
                width: 130
            },
            {
                field: 'Actions',
                title: 'Ação',
                sortable: false,
                width: 80,
                overflow: 'visible',
                autoHide: false,
                class: 'kt-actions',
                template: function() {
                    return '\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-lg fas fa-edit text-primary"></i>\
                            </span>\
                        </a>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
                            <span class="svg-icon svg-icon-md">\
                                <i class="icon-md flaticon2-cross text-danger"></i>\
                            </span>\
                        </a>\
                    ';
                },
            }],

        });
    };

    return {
        // public functions
        init: function() {
            tableAreaPessoal();
            tableRelatorios();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableRemoteAjaxDemo.init();
});
